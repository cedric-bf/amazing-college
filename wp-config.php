<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_bluefeet' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HA|^Ob4P50$tC:+-jsE_ZiB9D`=1O8EGlo7fSB7(LfUd/p:fQ Ms>zUF}lZ.ASFs');
define('SECURE_AUTH_KEY',  '#cy=pz;,d9hO22 (8- C[8l?=>~`UPC2C%m&giy_^+_@tawNP2Cp%NLyMz&MD]+|');
define('LOGGED_IN_KEY',    '],%]o`{}Qjqc6Hbu6>s^SG|cx=R-@+7Sm8^;0Z>#W}(OGt3_d|F|Py|uq5P5/p.U');
define('NONCE_KEY',        '}d=vP+Db/II5RpJ$!:={mQ:by5|@-dgR#ouF1{k+b~ %aNJY+YcA1!}TM5bFw.i.');
define('AUTH_SALT',        'DUZbJsQ)BuG@mGQ*dX4n--Wd.C,a.}:L5_bOM+I,BRcaAn<Kh!_Z|>2aQ_q}ZSod');
define('SECURE_AUTH_SALT', 'y1:p,A;qJ4xn`ede4D+z>CIu.)&Y[;PB:IdV6adw=$M1}V+qUW`Yw)^y&rI9E0co');
define('LOGGED_IN_SALT',   '-7YNQu(xqumlYH5_=%~t*!>J)u}n09#.z=$#K]{ke=>}#^d28i_:7@<|~) 2TO^d');
define('NONCE_SALT',       'chzu;$Br-{V(( DUJv5{J9$/rW |P^*.qw4(glq|RFeFy+[zI.,Sf0YX<A/o)k:B');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
